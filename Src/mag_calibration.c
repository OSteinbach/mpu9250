/*
 * mag_calibration.c
 *
 *  Created on: 07.06.2020
 *      Author: Ole
 */

#include "flash.h"
#include "mag_calibration.h"

/**----------------variables---------------------------*/
// default values

 float offset[3] = {0.0f, 0.0f, 0.0f};
 float gain[3] = {1.0f, 1.0f, 1.0f};
 float rotation[3][3] = {{1.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 1.0f}};
/**-----------------------------------------------------*/


/**------------------functions--------------------------*/
void read_calibration_data(void)
{
	flash_set_page_address(0x0801FC00);	// set flash page to last available in flash
	flash_read_block(0, (uint16_t *) offset, 6);	// get offsets
	flash_read_block(3, (uint16_t *) gain, 6);		// get gains
	flash_read_block(6, (uint16_t *) rotation, 18);	// get rotation
}

void write_calibration_data(void)
{
	// TODO
	flash_set_page_address(0x0801FC00);	// set flash page to last available in flash
	float calibration_values[15];
	for (uint8_t i = 0; i < 3; i++)
	{
		calibration_values[i] = offset[i];
		calibration_values[i + 3] = gain[i];
		calibration_values[i + 6] = rotation[0][i];
		calibration_values[i + 9] = rotation[1][i];
		calibration_values[i + 12] = rotation[2][i];
	}
	flash_write_block(0, (uint16_t *) calibration_values, 30);
}

void compensate_calibration(AxisData* data)
{
	float rot_x, rot_y, rot_z;

	/* remove hard iron effects by subtracting offsets*/
		data->x -= offset[0];
		data->y -= offset[1];
		data->z -= offset[2];

	/* remove soft iron effects. first step: rotation */
		rot_x = data->x * rotation[0][0] + data->y * rotation[0][1] + data->z * rotation[0][2];
		rot_y = data->x * rotation[1][0] + data->y * rotation[1][1] + data->z * rotation[1][2];
		rot_z = data->x * rotation[2][0] + data->y * rotation[2][1] + data->z * rotation[2][2];

	/* remove soft iron effects. second step: gains (calculated as factor) */
		data->x = rot_x * gain[0];
		data->y = rot_y * gain[1];
		data->z = rot_z * gain[2];
}
/**-----------------------------------------------------*/
