/*
 * mpu8250.h
 *
 *  Created on: 10.04.2020
 *      Author: Ole
 */

#include "mpu9250.h"
#include "mag_calibration.h"

/* Private variables */
AccelFS Accel_FS = FS_2G;
GyroFS Gyro_FS = FS_1000DPS;

/* Private functions */

/**
 * @brief	gets current Full Scale value for accelerometer from sensor and
 * 						sets internal Accel_FS value
 * @param	target pointer to i2c slave data
 */
void getAccelFS(I2CSlave* target)
{
	HAL_StatusTypeDef result;
	uint8_t buffer;
	result = readByte(target->hi2c, target->mpu_addr, ACCEL_CONFIG_ADDR, &buffer);
	if (result == HAL_OK)
		Accel_FS = (buffer >> 3) & 0x3;
}

/**
 * @brief 	gets current full scale value for gyroscope from sensor and
 * 						sets internal Gyro_FS value
 * @param 	target pointer to i2c slave data
 */
void getGyroFS(I2CSlave* target)
{
	HAL_StatusTypeDef result;
	uint8_t buffer;
	result = readByte(target->hi2c, target->mpu_addr, GYRO_CONFIG_ADDR, &buffer);
	if (result == HAL_OK)
		Gyro_FS = (buffer >> 3) & 0x3;
}

/**
 * @brief	Converts raw acceleration Data into values in m/s^2
 * @note	Conversion: res = value * Accel_FS / (0.5 * 2^16) * 9.81
 * @param 	accel_value Raw acceleration value
 * @retval	Value in m/s^2
 */
float accelToFS(int16_t accel_value)
{
	switch(Accel_FS)
	{
	case FS_2G:
		return accel_value * 5.98755e-4;
		break;
	case FS_4G:
		return accel_value * 1.19751e-3;
		break;
	case FS_8G:
		return accel_value * 2.39501e-3;
		break;
	case FS_16G:
		return accel_value * 4.79004e-3;
		break;
	default:
		return 0.0;
		break;
	}
}

/**
 * @brief	Converts raw gyroscope data into values in /s
 * @note	Conversion: res = value * Gyro_FS / (0.5 * 2^16)
 * @param 	gyro_value Raw gyroscope value
 * @retval	Value in /s
 */
float gyroToFS(int16_t gyro_value)
{
	switch(Gyro_FS)
	{
	case FS_250DPS:
		return gyro_value * 7.62939e-3;
		break;
	case FS_500DPS:
		return gyro_value * 15.25879e-3;
		break;
	case FS_1000DPS:
		return gyro_value * 30.51758e-3;
		break;
	case FS_2000DPS:
		return gyro_value * 61.03516e-3;
		break;
	default:
		return 0.0;
		break;
	}
}

/**
 * @brief	Converts the raw magnetometer data into values in T
 * @note	Conversion: res = value * 4800T / 2^13
 * @param	magnet_value Raw magnetometer value
 * @retval	Value in T
 */
float magnetToFS(int16_t magnet_value)
{
	return magnet_value * 0.14648;
}

/* Public functions */

/**
 * @brief	writes a single byte of data via I2C
 * @param 	hi2c i2c handle
 * @param 	slave_addr slave i2c address
 * @param	reg_addr Target register address
 * @param	data Value to be written
 * @retval  HAL Status
 */
HAL_StatusTypeDef writeByte(I2C_HandleTypeDef* hi2c, uint8_t slave_addr, uint8_t reg_addr, uint8_t data)
{
	HAL_StatusTypeDef result;
	uint8_t write_buffer[2];
	write_buffer[0] = reg_addr;
	write_buffer[1] = data;

	result = HAL_I2C_Master_Transmit(hi2c, slave_addr, write_buffer, 2, HAL_MAX_DELAY);
	return result;
}

/**
 * @brief	Reads a single Byte of Data from the mpu9250.
 * @param 	hi2c i2c handle
 * @param 	slave_addr slave i2c address
 * @param	reg_addr Target Register Address
 * @param	data_buffer Pointer to data buffer
 * @retval	HAL status
 */
HAL_StatusTypeDef readByte(I2C_HandleTypeDef* hi2c, uint8_t slave_addr, uint8_t reg_addr, uint8_t* data_buffer)
{
	HAL_StatusTypeDef result;

	result = HAL_I2C_Master_Transmit(hi2c, slave_addr, &reg_addr, 1, HAL_MAX_DELAY);
	if (result != HAL_OK)
		return result;
	result = HAL_I2C_Master_Receive(hi2c, slave_addr, data_buffer, 1, HAL_MAX_DELAY);

	return result;
}

/**
 * @brief	Reads the number of specified consecutive Bytes of Data from the mpu9250.
 * @param 	hi2c i2c handle
 * @param 	slave_addr slave i2c address
 * @param	reg_addr Target Register Address
 * @param 	size Number of Bytes to be read
 * @param	data_buffer Pointer to data buffer
 * @retval	HAL status
 */
HAL_StatusTypeDef readBytes(I2C_HandleTypeDef* hi2c, uint8_t slave_addr, uint8_t reg_addr, uint8_t size, uint8_t* data_buffer)
{
	HAL_StatusTypeDef result;
	result = HAL_I2C_Master_Transmit(hi2c, slave_addr, &reg_addr, 1, HAL_MAX_DELAY);
		if (result != HAL_OK)
			return result;
	result = HAL_I2C_Master_Receive(hi2c, slave_addr, data_buffer, size, HAL_MAX_DELAY);

	return result;
}

/**
 * @brief	Initialize sensor Configs for all sensors
 * @param 	target pointer to i2c slave data
 */
HAL_StatusTypeDef initMPU9250(I2CSlave* target)
{
	uint8_t res;
	HAL_StatusTypeDef status;

	// check if MPU9250 is ready for communication
	if (HAL_I2C_IsDeviceReady(target->hi2c, target->mpu_addr, 10, HAL_MAX_DELAY) != HAL_OK)
		return HAL_ERROR;

	//check communication to MPU9250 (read who am i byte)
	status = readByte(target->hi2c, target->mpu_addr, 0x75, &res);
	if(status != HAL_OK || res != 0x71)
		return HAL_ERROR;

	// initialize accelerometer full scale to 2g
	setAccelFS(target, Accel_FS);
	// initialize gyroscope full scale to 250dps
	setGyroFS(target, Gyro_FS);
	// initialize I2C Bypass to address Magnetometer values
	writeByte(target->hi2c, target->mpu_addr, 0x37, 0x02);

	// check communication to AK8963 (read who am i byte)
	status = readByte(target->hi2c, target->ak_addr, 0x0, &res);
	if(status != HAL_OK || res != 0x48)
		return HAL_ERROR;

	return HAL_OK;
}

/**
 * @brief 	sets the Full Scale value for accelerometer
 * @param	target pointer to i2c slave data
 * @param	value new Full Scale value. Depends on AccelFS enum
 */
void setAccelFS(I2CSlave* target, AccelFS value)
{
	writeByte(target->hi2c, target->mpu_addr, ACCEL_CONFIG_ADDR, (value << 3));
}

/**
 * @brief	Returns the Acceleration data for x, y, z-Axis in m/s^2
 * @param	target pointer to i2c slave data
 * @param	accel_data Struct that the result is assigned to
 */
void getAccelData(I2CSlave* target, AxisData* accel_data)
{
	uint8_t temp_buffer[6];
	readBytes(target->hi2c, target->mpu_addr, ACCEL_ADDR, 6, temp_buffer);
	accel_data->x = accelToFS((int16_t) (temp_buffer[0] << 8 | temp_buffer[1]));
	accel_data->y = accelToFS((int16_t) (temp_buffer[2] << 8 | temp_buffer[3]));
	accel_data->z = accelToFS((int16_t) (temp_buffer[4] << 8 | temp_buffer[5]));
}

/**
 * @brief	sets the full scale value for gyroscope
 * @param	target pointer to i2c slave data
 * @param 	value new full scale value. Depends on GyroFS enum
 */
void setGyroFS(I2CSlave* target, GyroFS value)
{
	writeByte(target->hi2c, target->mpu_addr, GYRO_CONFIG_ADDR, (value << 3));
}

/**
 * @brief	Returns the gyroscope data for x, y, z axis in /s
 * @param	target pointer to i2c slave data
 * @param	gyro_data Struct that the result is assigned to
 */
void getGyroData(I2CSlave* target, AxisData* gyro_data)
{
	uint8_t temp_buffer[6];
	readBytes(target->hi2c, target->mpu_addr, GYRO_ADDR, 6, temp_buffer);
	gyro_data->x = gyroToFS((int16_t) (temp_buffer[0] << 8 | temp_buffer[1]));
	gyro_data->y = gyroToFS((int16_t) (temp_buffer[2] << 8 | temp_buffer[3]));
	gyro_data->z = gyroToFS((int16_t) (temp_buffer[4] << 8 | temp_buffer[5]));
}

/**
 * @brief	Returns the magnetometer data for x, y, z axis in T
 * @param 	target pointer to i2c slave data
 * @param 	magnet_data Struct that the result is assigned to
 */
void getMagnetData(I2CSlave* target, AxisData* magnet_data)
{
	uint8_t res;
	uint8_t temp_buffer[6];

	// set single value, 16-bit
	writeByte(target->hi2c, target->ak_addr, 0x0A, 0x11);
	// wait until data is available
	do{
		readByte(target->hi2c, target->ak_addr, 0x02, &res);
	} while (res != 1);
	// read data
	readBytes(target->hi2c, target->ak_addr, 0x03, 6, temp_buffer);
	// check sensor overflow bit
	readByte(target->hi2c, target->ak_addr, 0x09, &res);
	if(!(res & 0x08))
	{
		magnet_data->x = magnetToFS((int16_t) (temp_buffer[3] << 8 | temp_buffer[2]));	// x and y axis of magnetometer are
		magnet_data->y = magnetToFS((int16_t) (temp_buffer[1] << 8 | temp_buffer[0]));	// switched compared to accelerometer
		magnet_data->z = -(magnetToFS((int16_t) (temp_buffer[5] << 8 | temp_buffer[4])));

		//compensate hard and soft iron effects
		compensate_calibration(magnet_data);
	}
}

/**
 * @brief 	reads sensor data of all sensors (accelerometer, gyroscope and magnetometer)
 * @param	target i2c pointer to i2c slave data
 * @param	sensor_data pointer to data struct to hold sensor data
 */
void getSensorData(I2CSlave* target, SensorData* sensor_data)
{
	getAccelData(target, &sensor_data->accel_data);
	getGyroData(target, &sensor_data->gyro_data);
	getMagnetData(target, &sensor_data->magn_data);
}
