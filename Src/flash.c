#include "flash.h"
/********************************************************/
/*
Small Library to save data on internal flash memory.

author: Tim Ole Steinbach
 */
/********************************************************/

/**----------------variables---------------------------*/
static uint32_t page_address;
/**-----------------------------------------------------*/

/**------------------functions--------------------------*/
void flash_erase_page(void)
{
	FLASH_EraseInitTypeDef eraseInit;
	eraseInit.TypeErase = FLASH_TYPEERASE_PAGES;
	eraseInit.Banks = FLASH_BANK_1;
	eraseInit.PageAddress = page_address;
	eraseInit.NbPages = 1;

	uint32_t pageError = 0;

	HAL_FLASH_Unlock();
	HAL_FLASHEx_Erase(&eraseInit, &pageError);
	HAL_FLASH_Lock();
}

void flash_set_page_address(uint32_t address)
{
	page_address = address;
}

/*
 * Block size is given in 16-Bit blocks!
 */
void flash_write_block(uint32_t idx, void* w_buffer, uint32_t block_size)
{
	uint32_t flash_address = page_address + idx;

	// erase page before writing
	flash_erase_page();

	// unlock flash
	HAL_FLASH_Unlock();
	//__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_OPTVERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGERR | FLASH_FLAG_BSY);

	// write data to flash
	for (uint32_t i = 0; i < block_size; i++)
	{
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, flash_address, ((uint16_t *)w_buffer)[i]);
		flash_address += 2;
	}

	// lock flash
	HAL_FLASH_Lock();
}

/*
 * Block_Size is given in 16-Bit blocks!
 */
void flash_read_block(uint32_t idx, void* r_buffer, uint32_t block_size)
{
	uint32_t flash_address = page_address + idx;


	for (uint32_t i = 0; i < block_size; i++)
	{
		*((uint16_t *) r_buffer + i) = *(uint16_t *) flash_address;
		flash_address += 2;
	}
}
/**-----------------------------------------------------*/
