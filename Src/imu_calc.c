 /*
  * imu_calc.h
  *
  *      Author: Johannes-Friedrich Knappe
  */

#include "imu_calc.h"
#include "tim.h"
#include "string.h"
#include "kalman.h"
#include <math.h>

/**----------------variables---------------------------*/
// mathematical constants
	#define g  9.81
// -----

	float mag_decl = 0.062; // magnetic declination in east direction, jena = 0.62 ~ 3.55�

	double div180pi, r180pi;

	struct kalman_data KalmanData_accel;
	struct kalman_data KalmanData_mag;
	float covar_accel[3] = {500000, 50.5723, 5.05723};
	float covar_mag[3] = {55000, 58.5221, 5.085221};
// ----

// data and scaling
	struct Dataset{
	// raw
	SensorData imu_data_raw; // includes x/y/y-vector for each sensor
	// ----

	// computed
	SensorData imu_data;
	AxisData rot; // single x/y/y-vector
	double heading_north; // heading
	// ----

		// management
			uint32_t sysTick; // time stamp of last sampled data
			double delta_seconds;
		// ----
	}Dataset,*data;


	AxisData	*h_meas   = &Dataset.imu_data.accel_data;
	AxisData	*g_sens   = &Dataset.imu_data.gyro_data;
	AxisData	*magnet   = &Dataset.imu_data.magn_data;

	double *head_n;
	AxisData h, n, o; // rotation
	double res_h, res_n, res_m, nk, res_nk, res_n_meas; // vector scaling
	AxisData h_est, h_diff,  n_est, n_meas, tm; // directional vectors
	float course_rad, course_deg;
	SensorData imu_data_old;
// -----

// IMU
	I2CSlave target;
	HAL_StatusTypeDef status_mpu;
// -----

// time base and process management
	volatile uint32_t sysTickCounter = 0;
// -----
/**-----------------------------------------------------*/


/**------------------functions--------------------------*/
/*
 * initalise all peripheral devices and set internal registers
 */
void sensors_init(void){
	// prepare mathem. constants
		div180pi = 180 / M_PI;
		r180pi = 6371000 / div180pi;
	// ----- //

	// initalise all peripheral devices and processes //
		HAL_TIM_Base_Start_IT(&htim1);// start internal timer for systick
		HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1); // start continuous PWM generation for rudder servo

		// initalise mpu
			target = (I2CSlave) {&hi2c1, MPU9250_ADDR, AK8963_ADDR};
			status_mpu = initMPU9250(&target);
			if (status_mpu != HAL_OK)
				Error_Handler();
		// -----

		// initalise Kalman filter
			getSensorData(&target, &Dataset.imu_data);
			kalman_init(&Dataset.imu_data.accel_data, &KalmanData_accel, &covar_accel);
			kalman_init(&Dataset.imu_data.magn_data, &KalmanData_mag, &covar_mag);
		// -----
	// ----- //
}

/*
 * read sensor data, run Kalman filter and set course to rudder servo
 * @retval HAL Debug message
 */
int sensors(void){
	// read sensor data //
	getSensorData(&target, &Dataset.imu_data); // polling from mpu
	// ----- //


	// main routine //
	if( memcmp(&imu_data_old, &Dataset.imu_data, sizeof(SensorData))){ // returns number !=0 when new data is incomming
		imu_data_old = Dataset.imu_data;

		Dataset.delta_seconds = (sysTickCounter - Dataset.sysTick) / 1000.0;
		Dataset.sysTick = sysTickCounter; // set marker for sampling to data-struct

		calc_imu(); // calculate position
		calc_ruder((double) course_deg); // set rudder servo
		}
	// ----- //
	return HAL_OK;
}

/*
 * apply course calculation
 */
void calc_imu() {
	// apply Kalman filter //
		// predict filter
			k_predict(g_sens, Dataset.delta_seconds, &KalmanData_accel);
			k_predict(g_sens, Dataset.delta_seconds, &KalmanData_mag);
		// -----

		// compensate filter
			k_update(h_meas, Dataset.delta_seconds, &KalmanData_accel);

			// compensate inclination
			korr_inclination(&KalmanData_accel.x, magnet);
			// -----

			k_update(magnet, Dataset.delta_seconds, &KalmanData_mag);
		// -----


		// compute course
			course_rad = 2*M_PI - calc_course(&KalmanData_accel.x, &KalmanData_mag.x) + mag_decl; // mag_decl compensates declination in east direction
        	course_deg = fmod(course_rad, 2*M_PI) * div180pi; // convert to degree for rudder control
		// -----
	// ----- //

}
/*-----------------------------------------------------*/
