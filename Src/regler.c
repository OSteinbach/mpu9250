/*
 * regler.c
 *
 *  Created on: 28.05.2020
 *      Author: Martin Albrecht, Johannes-Friedrich Knappe
 */
#include <regler.h>

/**----------------variables---------------------------*/
#define r_max		85.0								// max rudder angle, -r_max < r < r_max, 0� Servo ~> -r_max, 180� Servo ~> +r_max
#define dk_sat		20.0								// minimal delta of course to set maximum rudder gain
#define p			r_max / dk_sat						// amplification for p-controller
#define pwm_t_0		50									// time in 1 ms * 100 of dutycycle, required for 0� angle
#define pwm_t_180	2500								// -||-, required for 180� angle
#define pwm_delta   (pwm_t_180 - pwm_t_0 * 1. ) / 180	// calculate duty cycle per degree
#define rud_con		1									// conversion factor between rudder mechanics and servo (= rudder in degree / servo in degree)

double k_soll = 66.6;

/* 				     0� (rudder)
 *    				90� (servo)
 *    				/\
 *	 90� (rudder) --||-- -90� (rudder)
 *	180� (servo)  | || |   0� (servo)
 *				  | () |
 *				  |    |
 *				  ------
 *  			  servo
 *
 *  The rudder uses an internaly diffrent orientaion than the servo motor.
 *  The rudder system will be used until the course is set as servo-compatible value in the pwm register.
 */

/**-----------------------------------------------------*/


/**------------------functions--------------------------*/
int set_sails (double course){
	if(course < 360.0){
		k_soll = course;
		return 1;
	}
	return 0;
}

/*
 * returns delta between current and given course
 */
double calc_diff (double k_ist){
	double k_diff = 0.0;

	k_diff = k_soll-k_ist;

	if (k_diff > 180.0){			// correct course if 180� exeeded
		k_diff = k_diff - 360.0;
	} else if (k_diff < -180.0) {
		k_diff = k_diff + 360.0;
	}

	return k_diff;
}

void calc_ruder (double k_ist){
	double ruder = 0.0;

	ruder = (p * calc_diff (k_ist));

	if (ruder > r_max){				// limits rudder to +- r_max
		ruder =  r_max;
	} else if (ruder < -r_max){
		ruder =  -r_max;
	}

	// rudder dutycycle = (rudder in degree + offset between orientations) * mechanical conversion factor * pwm value per degree + offset for 0� duty cycle
	uint16_t r_pwm = (uint16_t) ((ruder + 90) * rud_con * pwm_delta + pwm_t_0);
	htim2.Instance->CCR1 = r_pwm; // write duty cycle to timer register

}
/**-----------------------------------------------------*/

