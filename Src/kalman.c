/*
 * kalman.c
 *
 *  Created on: 11.08.2020
 *      Author: Martin Albrecht
 */

#include "kalman.h"
#include "stm32f1xx.h"
#include <math.h>

/* Initialisierung der Kalman-Filter
 *
 * @param x_Init=[x,y,z]:					1. Messwert von ACC/MAG als Initialzustand
 * @param KalmanData: 						Datenstruckt des Kalman-Filters
 * @param covar=[r_Meas, q_pred, q_gyr]:	Werte f�r R und Q
 */

void kalman_init(float* x_Init, struct kalman_data* KalmanData, float* covar){
	for (uint8_t i = 0; i < 4; i++)
	{
		KalmanData->P[i] = 0;
		KalmanData->pred_P[i] = 0;
	}


	for (uint8_t i = 0; i < 3; i++)
	{
		KalmanData->pred_x[i] = 0;
		KalmanData->bias[i] = 0;
		KalmanData->x[i] = x_Init[i];
	}

	KalmanData->GainDivOld = 1;

	KalmanData->r_Meas = covar[0];
	KalmanData->q_pred = covar[1];
	KalmanData->q_gyr  = covar[2];
}

/*********************************************************************************************/

/* Vorhersageschritt des Kalman-Filters
 *
 * @param gyro=[x,y,z]: Drehratenwerte des Gyroskopsensors
 * @param delta_t:		verstrichene Zeit seit letzter Messung
 * @param KalmanData:	Datenstruckt des Kalman-Filters
 */

void k_predict(float* gyro, float delta_t, struct kalman_data* KalmanData){
	/* Berechnen des �berstrichenen Winkels
	 *
	 * Angabe im Bogenma�, Korrektur der Drift duch Subtraktion des Bias
	 */
	KalmanData->diff[0] = (gyro[0] - KalmanData->bias[0])*delta_t*M_PI/180.0;
	KalmanData->diff[1] = (gyro[1] - KalmanData->bias[1])*delta_t*M_PI/180.0;
	KalmanData->diff[2] = (gyro[2] - KalmanData->bias[2])*delta_t*M_PI/180.0;


	/* Vorhersage der Achsenwerte
	 *
	 * Berechnung aus den letzten Korrekturwerten und dem �berstrichenen Winkel
	 * Normierung des Betrags auf 1
	 *
	 * Bei bedarf Tangens weglassen (da nur kleine Winkel, tan nahe 0 linear)
	 */
	KalmanData->pred_x[0] = 						  KalmanData->x[0] + tan(KalmanData->diff[2])		*	KalmanData->x[1] - tan(KalmanData->diff[1])	*	KalmanData->x[2];
	KalmanData->pred_x[1] = -tan(KalmanData->diff[2])*KalmanData->x[0] + 			  	  					KalmanData->x[1] + tan(KalmanData->diff[0])	*	KalmanData->x[2];
	KalmanData->pred_x[2] =  tan(KalmanData->diff[1])*KalmanData->x[0] - tan(KalmanData->diff[0])  		*	KalmanData->x[1] +								KalmanData->x[2];

	float normDiv = sqrt(((KalmanData->pred_x[0])*(KalmanData->pred_x[0]) + (KalmanData->pred_x[1])*(KalmanData->pred_x[1]) + (KalmanData->pred_x[2])*(KalmanData->pred_x[2])));
	KalmanData->pred_x[0] = KalmanData->pred_x[0]/normDiv;
	KalmanData->pred_x[1] = KalmanData->pred_x[1]/normDiv;
	KalmanData->pred_x[2] = KalmanData->pred_x[2]/normDiv;

	/* An dieser Stelle w�rde im Allgemeinen eine Vorhersage des Bias erfolgen.
	 * Es wird jedoch angenommen, dass sich die Drift, und damit auch der Bias, zwischen zwei Messungen
	 * ann�hernd konstant verh�lt, sodass der Vorhersagewert gleich dem vorherigen Korrekturwert ist.
	 */

	/* Vorhersage der Fehlercovarianzmatrix */

	KalmanData->pred_P[0] = KalmanData->P[0] - ((KalmanData->P[1] + KalmanData->P[2] - KalmanData->P[3]*delta_t)*delta_t) + KalmanData->q_pred;
	KalmanData->pred_P[1] = KalmanData->P[1] - KalmanData->P[3]*delta_t;
	KalmanData->pred_P[2] = KalmanData->P[2] - KalmanData->P[3]*delta_t;
	KalmanData->pred_P[3] = KalmanData->P[3] + KalmanData->q_gyr;
}

/*********************************************************************************************/

/* Korrekturschritt des Kalman-Filters
 *
 * @param meas=[x,y,z]: Messwerte des Beschleunigungs-/Magnetfeldsensors
 * @param KalmanData: 	Datenstruckt des Kalman-Filters
 */

void k_update(float* meas, float delta_t, struct kalman_data* KalmanData){
	float y[3];

	/* Berechnen der Differenz zwischen Vorhersage und Messung
	 *
	 * Normieren der eingehenden Messwerte zur Anpassung an Vorhersage
	 */
	float normDiv = sqrt(((meas[0]*meas[0])+(meas[1]*meas[1])+(meas[2]*meas[2])));
	y[0] = meas[0]/normDiv - KalmanData->pred_x[0];
	y[1] = meas[1]/normDiv - KalmanData->pred_x[1];
	y[2] = meas[2]/normDiv - KalmanData->pred_x[2];

	/* Berechnen der Kalman-Verst�rkung
	 *
	 * Ausgangspunkt: P und R
	 * Nenner auf 0 pr�fen und gegebenenfalls durch vorherigen Wert ersetzen
	 */
	float GainDiv = KalmanData->pred_P[0] + KalmanData->r_Meas;
	if (GainDiv == 0)
		GainDiv = KalmanData->GainDivOld;
	else
		KalmanData->GainDivOld = GainDiv;
	float K0 = KalmanData->pred_P[0]/GainDiv;
	float K1 = KalmanData->pred_P[2]/GainDiv;

	/* Korrektur der Lage
	 *
	 * Berechnet als mit K0 gewichtete Summe von Vorhersage und Messung
	 * Anschlie�end Normierung auf 1
	 */
	KalmanData->x[0] = KalmanData->pred_x[0] + K0*y[0];
	KalmanData->x[1] = KalmanData->pred_x[1] + K0*y[1];
	KalmanData->x[2] = KalmanData->pred_x[2] + K0*y[2];

	float normDivX = sqrt(((KalmanData->x[0])*(KalmanData->x[0]) + (KalmanData->x[1])*(KalmanData->x[1]) + (KalmanData->x[2])*(KalmanData->x[2])));
	KalmanData->x[0] = KalmanData->x[0]/normDivX;
	KalmanData->x[1] = KalmanData->x[1]/normDivX;
	KalmanData->x[2] = KalmanData->x[2]/normDivX;

	/* Berechnen des Vorhersage-Bias
	 *
	 * Berechent aus vorherigem Bias und aktueller Abweichung zwischen den �berstrichenen Winkeln
	 */

	float angle[3];	// = [nick, roll, yaw] aus aktueller Messung
	float diffAngle[3]; // = [dNick, dRoll, dYaw] �berstrichener Winkel zw. den Messungen

	angle[0] = atan2(KalmanData->x[2], KalmanData->x[1]);
	angle[1] = atan2(KalmanData->x[0], KalmanData->x[2]);
	angle[2] = atan2(KalmanData->x[1], KalmanData->x[0]);

	diffAngle[0] = angle[0] - KalmanData->angleOld[0];
	diffAngle[1] = angle[1] - KalmanData->angleOld[1];
	diffAngle[2] = angle[2] - KalmanData->angleOld[2];

	KalmanData->angleOld[0] = angle[0];
	KalmanData->angleOld[1] = angle[1];
	KalmanData->angleOld[2] = angle[2];

	KalmanData->bias[0] = KalmanData->bias[0] + K1*(diffAngle[0] - KalmanData->diff[0])/delta_t;
	KalmanData->bias[1] = KalmanData->bias[1] + K1*(diffAngle[1] - KalmanData->diff[1])/delta_t;
	KalmanData->bias[2] = KalmanData->bias[2] + K1*(diffAngle[2] - KalmanData->diff[2])/delta_t;

	/* Korrektur der Fehlercovarianzmatrix */
	KalmanData->P[0] = KalmanData->pred_P[0] - K0*KalmanData->pred_P[0];
	KalmanData->P[1] = KalmanData->pred_P[1] - K0*KalmanData->pred_P[1];
	KalmanData->P[2] = KalmanData->pred_P[2] - K1*KalmanData->pred_P[0];
	KalmanData->P[3] = KalmanData->pred_P[3] - K1*KalmanData->pred_P[1];
}

/*********************************************************************************************/

/* Korrektur der magnetischen Inklination
 *
 * mit jedem Messwert des Magnetsensors durchzufhren
 *
 * mag_korr = mag_norm - cos(90-Inklination)*acc_norm
 *
 * @param acc=[x,y,z]:	Lagevectoren ber Beschleunigungssensor, nach Kalman-Filter
 * @param mag=[x,y,z]:	Sensordaten des Magnetfeldsensors, vor Kalman-Filter
 * @param inc:			Inklinationswert in rad
 */
void korr_inclination(float* acc, float* mag){
	float normDivM = sqrt(((mag[0]*mag[0])+(mag[1]*mag[1])+(mag[2]*mag[2])));

	mag[0] = mag[0]/normDivM + 0.906308*acc[0];
	mag[1] = mag[1]/normDivM + 0.906308*acc[1];
	mag[2] = mag[2]/normDivM + 0.906308*acc[2];
}

/*********************************************************************************************/

/* Kursberechnung
 *
 * Kurs als Winkel zwischen Nordrichtung und in N-O-Ebene projiziertem y-Einheitsvektor als Fahrtrichtung
 *
 * @param height=[x,y,z]: h-Vector in 3 Komponenten
 * @param north=[x,y,z]:  N-Vector in 3 Komponenten
 *
 * @retval: Kurs als Float in rad
 */
float calc_course(float* height, float* north){
	float east[3], y_proj[3];
	/* Berechnen des Ost-Vectors als Kreuzprodukt Nord x Hoehe */
	east[0] = north[1]*height[2] - north[2]*height[1];
	east[1] = north[2]*height[0] - north[0]*height[2];
	east[2] = north[0]*height[1] - north[1]*height[0];

	/* Normierungsfaktoren f�r Nord- und Ostvektor */
	 float normDivE = sqrt(((east[0]*east[0])+(east[1]*east[1])+(east[2]*east[2])));

	/* Projektion des y-Einheitsvektors in N-O-Ebene */
	float dotP_yn = north[1];	// Skalarprodukt aus lokalem y-Einheitsvector und globalem N-Einheitsvector = y-Komponente des N-Einheitsvectors
	float dotP_ye = ((normDivE == 0) ?  0 :  east[1]/normDivE);	// Skalarprodukt aus lokalem y-Einheitsvector und globalem O-Einheitsvector = y-Komponente des O-Einheitsvectors

	y_proj[0] = dotP_yn*north[0] + dotP_ye*east[0]/normDivE;
	y_proj[1] = dotP_yn*north[1] + dotP_ye*east[1]/normDivE;
	y_proj[2] = dotP_yn*north[2] + dotP_ye*east[2]/normDivE;

	float normDivY = sqrt(((y_proj[0]*y_proj[0])+(y_proj[1]*y_proj[1])+(y_proj[2]*y_proj[2])));

	if (north[0] < 0) return acos(north[1]/normDivY);
	else return 2*M_PI - acos(north[1]/normDivY);
}

