/*
 * kalman.h
 *
 *  Created on: 11.08.2020
 *      Author: Martin Albrecht
 */

#ifndef KALMAN_H_
#define KALMAN_H_

struct kalman_data{
	/* Covarianzmatrizen
	 *
	 *  @param R=[r_Meas]:				 Covarianzmatrix des Messfehlers
	 *  @param Q=[[q_pred 0],[0 q_gyr]]: Covarianzmatrix des Modellfehlers
	 *  @param P=[[p00 p01],[p10 p11]]:  Covarianzmatrix des Vorhersagefehlers
	 */
	float r_Meas;
	float q_pred;
	float q_gyr;
	float P[4];

	/* Vorhersage-Daten
	 *
	 * @param pred_x=[x,y,z]:				Vorhersagewert der Lagevectoren aus Gyro-Signal
	 * @param pred_P=[[p00 p01],[p10 p11]]: Vorhersage der P-Matrix
	 * @param diff=[dNick,dRoll,dYaw]:		zuletzt �berstrichener Winkel --> Gyro * delta_t
	 */
	float pred_x[3];
	float pred_P[4];
	float diff[3];

	/* Korrektur-Daten
	 *
	 * @param GainDivOld:				  Divisor f�r Berechnung der Kalman-Verst�rkung, Vermeiden der Division durch Null
	 * @param x=[x,y,z]:				  Korrigierter Wert der Lagevectoren durch Messung
	 * @param bias=[bx, by, bz]:          Vorhersage-Bias, R�ckgabewert f�r Verbesserung der Vorhersage
	 * @param angleOld=[nick, roll, yaw]: Lagewinkel nach letzter Messung
	 */
	float GainDivOld;
	float x[3];
	float bias[3];
	float angleOld[3];
};

/* Initialisierung der Kalman-Filter
 *
 * @param x_Init=[x,y,z]:				 1. Messwert von ACC/MAG als Initialzustand
 * @param KalmanData: 					 Datenstruckt des Kalman-Filters
 * @param covar=[r_Meas, q_pred, q_gyr]: Werte f�r R und Q
 */
void kalman_init(float* x_Init, struct kalman_data* KalmanData, float* covar);


/* Vorhersageschritt des Kalman-Filters
 *
 * @param gyro=[x,y,z]: Drehratenwerte des Gyroskopsensors
 * @param delta_t:		verstrichene Zeit seit letzter Messung
 * @param KalmanData:	Datenstruckt des Kalman-Filters
 */
void k_predict(float* gyro, float delta_t, struct kalman_data* KalmanData);



/* Korrekturschritt des Kalman-Filters
 *
 * @param meas=[x,y,z]: Messwerte des Beschleunigungs-/Magnetfeldsensors
 * @param delta_t: 		verstrichene Zeit zwischen Messungen
 * @param KalmanData:   KalmanData: Datenstruckt des Kalman-Filters
 */
void k_update(float* meas, float delta_t, struct kalman_data* KalmanData);


/* Korrektur der magnetischen Inklination
 *
 * mit jedem Messwert des Magnetsensors durchzufhren
 *
 * mag_korr = mag_norm - cos(90-Inklination)*acc_norm
 *
 * @param acc=[x,y,z]: Lagevectoren ber Beschleunigungssensor, nach Kalman-Filter
 * @param mag=[x,y,z]: Sensordaten des Magnetfeldsensors, vor Kalman-Filter
 */
void korr_inclination(float* acc, float* mag);


/* Kursberechnung
 *
 * Kurs als Winkel zwischen Nordrichtung und in N-O-Ebene projiziertem y-Einheitsvektor als Fahrtrichtung
 *
 * @param height=[x,y,z]: h-Vector in 3 Komponenten
 * @param north=[x,y,z]:  N-Vector in 3 Komponenten
 *
 * @retVal: Kurs als float in rad
 */
float calc_course(float* height, float* north);


#endif /* KALMAN_H_ */
