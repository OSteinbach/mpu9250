/*
 * regler.h
 *
 *  Created on: 28.05.2020
 *      Author: Martin Albrecht
 */

#ifndef REGLER_H_
#define REGLER_H_

/**----------------Includes-----------------------------*/
#include "stm32f1xx_hal.h"
#include "main.h"
#include "stddef.h"
#include "math.h"
#include "tim.h"
#include "i2c.h"
#include "gpio.h"
/**-----------------------------------------------------*/


/**------------------functions--------------------------*/
/*
 * set new desired course
 * returns a '0' when invalid value was given
 * @param course: course between 0� and 360� to geographic north
 */
int set_sails (double course);

/*
 * computes course correction and sets rudder servo register
 * @param k_ist: current course to geographic north
 */
void calc_ruder (double k_ist);
/**-----------------------------------------------------*/

#endif /* REGLER_H_ */
