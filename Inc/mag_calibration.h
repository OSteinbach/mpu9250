#ifndef mag_calibration_h
#define mag_calibration_h

/**----------------Includes-----------------------------*/
#include "mpu9250.h"
/**-----------------------------------------------------*/


/**------------------functions--------------------------*/
void read_calibration_data(void);	// get calibration values from flash storage
void write_calibration_data(void);	// write current calibration values to flash storage
void compensate_calibration(AxisData* data);
/**-----------------------------------------------------*/

#endif
