#ifndef flash_h
#define flash_h

/**----------------Includes-----------------------------*/
#include "stm32f1xx_hal.h"
/**-----------------------------------------------------*/

/**------------------functions--------------------------*/
/*
 * Erase a sector of flash memory specified in flash_set_page_address
 */
void flash_erase_page(void);

/*
 * Set memory sector to be written / deleted
 * @param address: start address of the sector
 * Example: 0x0801FC00 is the address of the last sector on STM32F103RB
 */
void flash_set_page_address(uint32_t address);

/*
 * Write data to the specified flash sector
 * @param idx:
 */
void flash_write_block(uint32_t idx, void* w_buffer, uint32_t block_size);
void flash_read_block(uint32_t idx, void* r_buffer, uint32_t block_size);
/**-----------------------------------------------------*/

#endif
