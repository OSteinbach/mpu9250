/*
 * mpu9250.h
 *
 *  Created on: 10.04.2020
 *      Author: Ole
 */

#ifndef MPU9250_H_
#define MPU9250_H_

/**----------------Includes-----------------------------*/
#include "stm32f1xx_hal.h"
/**-----------------------------------------------------*/


/**----------------variables---------------------------*/
#define AK8963_ADDR 	(0x0C << 1)
#define MPU9250_ADDR 	(0x68 << 1)

#define ACCEL_CONFIG_ADDR 0x1C
#define ACCEL_ADDR 	0x3B
#define GYRO_CONFIG_ADDR 0x1B
#define GYRO_ADDR 	0x43
#define MAGN_ADDR	0x03

typedef enum{
	FS_2G = 0,
	FS_4G,
	FS_8G,
	FS_16G
} AccelFS;

typedef enum{
	FS_250DPS = 0,
	FS_500DPS,
	FS_1000DPS,
	FS_2000DPS
} GyroFS;

typedef struct{
	I2C_HandleTypeDef* hi2c;
	uint8_t mpu_addr;
	uint8_t ak_addr;
} I2CSlave;

typedef struct{
	float x;
	float y;
	float z;
}AxisData;

typedef struct{
	AxisData accel_data;
	AxisData gyro_data;
	AxisData magn_data;
}SensorData;
/**-----------------------------------------------------*/


/**------------------functions--------------------------*/
HAL_StatusTypeDef initMPU9250(I2CSlave* target);
HAL_StatusTypeDef readByte(I2C_HandleTypeDef* hi2c, uint8_t slave_addr, uint8_t reg_addr, uint8_t* data_buffer);
HAL_StatusTypeDef readByte(I2C_HandleTypeDef* hi2c, uint8_t slave_addr, uint8_t reg_addr, uint8_t* data_buffer);
void setAccelFS(I2CSlave* target, AccelFS value);
void getAccelData(I2CSlave* target, AxisData* accel_data);
void setGyroFS(I2CSlave* target, GyroFS value);
void getGyroData(I2CSlave* target, AxisData* gyro_data);
void getMagnetData(I2CSlave* target, AxisData* magnet_data);
void getSensorData(I2CSlave* target, SensorData* sensor_data);
/**-----------------------------------------------------*/

#endif /* MPU9250_H_ */
