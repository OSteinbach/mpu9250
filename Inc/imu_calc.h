 /*
  * imu_calc.h
  *
  *      Author: Johannes-Friedrich Knappe
  */


#ifndef imu_calc_h
#define imu_calc_h
#ifdef __cplusplus
 extern "C" {
#endif

/**----------------Includes-----------------------------*/
#include "stm32f1xx_hal.h"
#include "main.h"
#include "stddef.h"
#include "math.h"
#include "tim.h"
#include "i2c.h"
#include "gpio.h"
#include "mpu9250.h"
#include "stdint.h"
#include "regler.h"
/**-----------------------------------------------------*/


/**------------------functions--------------------------*/
 /*
  * initialise all sensors and timers
  */
void sensors_init(void);

/*
 * sample data, calculate heading and set servo - main task
 */
int sensors(void);

/*
 * calculate heading via Kalman filter
 */
void calc_imu(void);

/*
 * get system time via clock register counter
 */
extern volatile uint32_t sysTickCounter;
/**-----------------------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif
